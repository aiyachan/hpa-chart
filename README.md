# A chart for abstract web app with autoscaling

## Explanation
This chart will make a deployment to use an HPA mechanism to upscale itself with the load. I am not sure that it will work "from the box", because I, honestly, didn't make a test of this chart, but I think that it may have only syntax issues. Of course, if it will be needed, I can easily modify it for the existing web app.

## Authors
Made by sferatime as a test task 4 of Bamboo
